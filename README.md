# Transcendence IWL Mod

Hello. This is the preliminary commit of the updated Transcendence Independent Weapon Labs mod. This is an adopted mod, with permission gracefully granted to bugfix and expand it by its original creator, SDW195. Though several bugs have been fixed, this mod should not currently be regarded as neccessarily safe to use, as the current commit was mostly for the sake of having a backup. Future commits will implement features and should usually be in a playable state.

## Installation:
Drop the files in your Transcendence data directory.

## Support:
Once released "as working", support will probably be best sourced either from raising an issue here or asking on the Transcendence forums.

## Roadmap:
Coming soon. In the meantime here's a list of just some of the features I'd like to implement:

* Four tiers of IWL station - Enthusiast, Advanced, Master and Savant
* Expanded flavour text, different for each station
* New station and item graphics (giving me an excuse to use Blender again)
* Missions to obtain examples of NotForSale/Alien weaponry for the gunsmiths to examine (similar to Dvalin). The first one of these for each tier would unlock/upgrade IWL membership, via an ID card, allowing the player to use the station weapon lab
* An expanded/enhanced component system, including gun muzzles that turn shots into cloud style projectiles, capacitors as an add-on during construction, and repeaters/beamers as a separate component from barrels, which themselves are single components that may be combined into standard patterns or any of several custom IWL patterns to be implemented using the newer configuration tag system
* The multitarget component as a separate (computer-based) component that necessitates one omni mount per weapon barrel
* A re-vamped blueprinting system that begins with the core weapon module and then uses that as a "power budget" from which damage and fire rate is resolved on a per-damage-type basis, with number of barrels, repeaters, beamers, and mounts
* Bonuses/Maluses like "Exceptional Efficiency" or "Not Quite Recoilless", especially when getting higher-tier stations to make lower-tier items and vice versa
* IWL stations beginning to sell copies of particularly successful weapons, and those weapons potentially turning up in randomly-generated ships in the playthrough
* A custom disassembly display a-la the Tinkers, where the player can see a readout of potential components before even confirming each disassembly task
* Enhancement reclamation, with a chance of success based on station level

Currently, progress can be viewed here: https://trello.com/b/jUSs7yR0/transcendence-independent-weapon-labs-mod

## Contributions:
Not currently open.

## Authors and acknowledgment:
Many thanks to SDW195 for the original creation of the mod and to DigDug for helping me arrange my current management of it.

## License:
Contents of the the Transcendence Independent Weapons Lab Mod (hereafter referred to as "the Mod") may be freely reused and altered by users. The Mod and any altered versions of the Mod are not to be sold or used for any commercial purpose. All Mod code derivative of Transcendence core code and/or the work of George Moromisato et al defaults to the licenses of those works. If the Mod is forked or re-hosted, credit must be given to those previously involved in working on the Mod and most importantly to its creator, SDW195.

## Project status:
On hold while I'm still dealing with setting up my new working environment and home server systems.